# TWiki Improved #

Trims down the unnecessary whitespace from the TWiki editor, so that you have more screen space for typing content.

Also disables the annoying 'Error connecting to SSO' alert if you've left the editor open for > 5 mins.

### Installing ###

// TODO: document the install process for people who don't know how to use non-compiled chrome plugins