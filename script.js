document.getElementById("patternBottomBar").style.display = 'none';
document.getElementsByClassName("patternTemplateTitle")[0].style.display = 'none';

var topic = document.getElementsByClassName("patternTopic")[0];
topic.style.margin = '0';

for(var i in topic.childNodes){
	var child = topic.childNodes[i];
	if(child.tagName == 'BR'){
		child.style.display = 'none';
	}
}

document.getElementById("topic_toolbar1").style.display = 'inline-block';
var topic_toolbar2 = document.getElementById("topic_toolbar2");
topic_toolbar2.style.display = 'inline-block';
topic_toolbar2.style.marginLeft = '26px';

var mainContents = document.getElementById("patternMainContents");
mainContents.style.margin = '0';
mainContents.style.padding = '0';

var topic_tbl = document.getElementById("topic_tbl");
topic_tbl.style.width = '100%';
topic_tbl.style.height = '100%';

var div = document.getElementById("patternScreen");
for(var i = 0; i < 8; i++){
	div.style.height = 'inherit';
	div = div.firstElementChild;
}
document.getElementById("topic_parent").style.height = 'inherit';
document.getElementsByName("main")[0].style.height = 'inherit';

var topic = document.getElementById("topic");
topic.style.width = '100%';
topic.style.border = 'none';
topic.style.padding = '0';
topic.style.height = (window.innerHeight - 41) + "px";

document.getElementById("formHolder").style.height = (window.innerHeight - 22) + "px";
document.getElementById("topic_ifr").style.height = '100%';


var sigLine = document.getElementsByClassName("patternSigLine")[0];
sigLine.style.padding = '0';
sigLine.style.position = 'absolute';
sigLine.style.right = '0';

var topicActions = document.getElementsByClassName("patternTopicActions")[0];
topicActions.style.margin = '0';
topicActions.style.border = 'none';
var topicAction = document.getElementsByClassName("patternTopicAction")[0];
topicAction.style.padding = '0px 12px';
topicAction.style.border = 'none';
document.getElementsByClassName("patternActionButtons")[0].style.display = 'inline-block';
var saveOptions = document.getElementsByClassName("patternSaveOptions")[0];
saveOptions.style.display = 'inline-block';
saveOptions.style.margin = '0px 28px';

document.getElementById("topic_tbl").firstChild.lastChild.style.display = 'none';
document.getElementsByClassName("patternSaveHelp")[0].style.display = 'none';
document.getElementById("quietsave").style.display = 'none';
if(typeof document.getElementsByClassName("twikiChangeFormButtonHolder")[0] != 'undefined'){
   document.getElementsByClassName("twikiChangeFormButtonHolder")[0].style.display = 'none';
}
if(typeof document.getElementsByClassName("twikiClear")[1] != 'undefined'){
   document.getElementsByClassName("twikiClear")[1].style.display = 'none';
} else if(typeof document.getElementsByClassName("twikiClear")[0] != 'undefined'){
   document.getElementsByClassName("twikiClear")[0].style.display = 'none';
}


window.setTimeout(function() {
	var id = window.setTimeout(function() {}, 0);
  	console.log('clearing ' + id + ' timeouts...');
	while (id--) {
   	window.clearTimeout(id);
	}
}, 10000);
